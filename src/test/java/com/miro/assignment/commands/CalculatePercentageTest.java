package com.miro.assignment.commands;

import com.miro.assignment.model.EventType;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;

public class CalculatePercentageTest {

    @Test
    public void calculatePercentageTest() {
        final SparkSession sparkSession = SparkSession
                .builder()
                .appName("test")
                .master("local")
                .getOrCreate();

        final Dataset<Row> dataset = ExampleDataset.createTestDataset(sparkSession);

        final Dataset<Row> registeredDataset = Split.filterDataset(dataset, EventType.registered);
        final Dataset<Row> appLoadedDataset = Split.filterDataset(dataset, EventType.app_loaded);

        final double percentage = Calculate.calculatePercentage(sparkSession, registeredDataset, appLoadedDataset);
        final double expectedPercentage = 40; // from the example dataset, two users fits the calculate criteria

        assert(percentage == expectedPercentage);

        sparkSession.stop();
    }

}

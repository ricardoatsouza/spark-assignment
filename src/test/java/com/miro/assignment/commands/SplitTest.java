package com.miro.assignment.commands;

import com.miro.assignment.model.EventType;
import org.apache.spark.sql.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SplitTest {

    @Test
    public void filterTest() {
        final SparkSession sparkSession = SparkSession
                .builder()
                .appName("test")
                .master("local")
                .getOrCreate();

        final Dataset<Row> dataset = ExampleDataset.createTestDataset(sparkSession);

        final Dataset<Row> registeredDataset = Split.filterDataset(dataset, EventType.registered);
        final Dataset<Row> appLoadedDataset = Split.filterDataset(dataset, EventType.app_loaded);

        final List<String> columnsRegistered = Arrays.asList(registeredDataset.columns());
        final List<String> columnsAppLoaded = Arrays.asList(appLoadedDataset.columns());

        assert(!columnsRegistered.contains("browser_version"));
        assert(!columnsRegistered.contains("device_type"));
        assert(!columnsRegistered.contains("campaign"));

        assert(!columnsRegistered.contains("browser_version"));
        assert(!columnsAppLoaded.contains("campaign"));
        assert(!columnsAppLoaded.contains("channel"));

        final int expectedRegistered = 6;
        final int expectedAppLoaded = 5;

        assert(registeredDataset.count() == expectedRegistered);
        assert(registeredDataset.filter("event = 'registered'").count() == expectedRegistered);

        assert(appLoadedDataset.count() == expectedAppLoaded);
        assert(appLoadedDataset.filter("event = 'app_loaded'").count() == expectedAppLoaded);

        sparkSession.stop();
    }

}

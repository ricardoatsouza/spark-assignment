package com.miro.assignment.commands;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.List;

public abstract class ExampleDataset {
    public static Dataset<Row> createTestDataset(final SparkSession sparkSession) {
        /**
         * The following is a very simple dataset. It has:
         *  - 6 registered events
         *  - 5 app_loaded events
         *
         * As for user behaviours:
         *  - 1 and 4 opened the app one week calendar after registration
         *  - 2 registered and opened the app in the same day
         *  - 3 registered on a Monday and opened the app on Sunday (therefore, same calendar week)
         *  - 5 registered and opened the app a month apart
         *  - 6 only registered
         *
         */
        return sparkSession.createDataFrame(List.of(
                RowFactory.create("1", "2020-01-08T06:21:14.000Z", "registered", "some-device-A", "some-browser-A", "some-channel-A", "some-campaign"),
                RowFactory.create("2", "2020-01-05T05:21:14.000Z", "registered", "some-device-B", "some-browser-B", "some-channel-B", "some-campaign"),
                RowFactory.create("1", "2020-01-15T06:21:14.000Z", "app_loaded", "some-device-C", "some-browser-C", "some-channel-C", "some-campaign"),
                RowFactory.create("3", "2020-01-06T06:21:14.000Z", "registered", "some-device-D", "some-browser-D", "some-channel-D", "some-campaign"),
                RowFactory.create("2", "2020-01-05T12:21:14.000Z", "app_loaded", "some-device-E", "some-browser-E", "some-channel-E", "some-campaign"),
                RowFactory.create("3", "2020-01-12T06:21:14.000Z", "app_loaded", "some-device-F", "some-browser-F", "some-channel-F", "some-campaign"),
                RowFactory.create("4", "2020-01-06T06:21:14.000Z", "registered", "some-device-F", "some-browser-F", "some-channel-F", "some-campaign"),
                RowFactory.create("4", "2020-01-13T06:21:14.000Z", "app_loaded", "some-device-F", "some-browser-F", "some-channel-F", "some-campaign"),
                RowFactory.create("5", "2020-01-06T06:21:14.000Z", "registered", "some-device-F", "some-browser-F", "some-channel-F", "some-campaign"),
                RowFactory.create("5", "2020-02-13T06:21:14.000Z", "app_loaded", "some-device-F", "some-browser-F", "some-channel-F", "some-campaign"),
                RowFactory.create("6", "2020-03-06T06:21:14.000Z", "registered", "some-device-F", "some-browser-F", "some-channel-F", "some-campaign")
            ), new StructType(new StructField[]{
                new StructField("initiator_id", DataTypes.StringType, true, Metadata.empty()),
                new StructField("timestamp", DataTypes.StringType, true, Metadata.empty()),
                new StructField("event", DataTypes.StringType, true, Metadata.empty()),
                new StructField("device_type", DataTypes.StringType, true, Metadata.empty()),
                new StructField("browser_version", DataTypes.StringType, true, Metadata.empty()),
                new StructField("channel", DataTypes.StringType, true, Metadata.empty()),
                new StructField("campaign", DataTypes.StringType, true, Metadata.empty()),
        }));
    }

}

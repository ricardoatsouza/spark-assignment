package com.miro.assignment;

import com.miro.assignment.commands.Calculate;
import com.miro.assignment.commands.Split;
import com.miro.assignment.model.Mode;
import org.apache.spark.sql.SparkSession;


public class Main {
    private static final String appName = "EventAnalyser";


    private static void printHelpAndExit() {
        System.err.printf("Usage: ./%s <mode>\n", appName);
        System.err.println("\tmode: [split | calculate]");
        System.err.println("\t\tsplit <filename>: splits an event file according to its event: \"registered\" or \"app_loaded\".");
        System.err.println("\tcalculate: users spark to calculate the percentage of users that opened the app the calendar week after registration.");
        System.exit(1);
    }

    private static void ensureValidMode(final Mode mode, final String[] args) {
        if (mode.equals(Mode.split) && args.length < 2) {
            System.err.println("Filename expected");
            printHelpAndExit();
        }
    }

    public static void main(final String[] args) {
        if (args.length < 1) {
            printHelpAndExit();
        }

        final Mode mode;

        SparkSession sparkSession = null;
        try {
            mode = Mode.valueOf(args[0]);
            ensureValidMode(mode, args);

            sparkSession = SparkSession.builder().appName(appName).getOrCreate();

            switch (mode) {
                case split -> Split.splitAndSaveDatabaseByEvent(sparkSession, args[1]);
                case calculate -> Calculate.calculatePercentage(sparkSession);
            }
        } catch (final IllegalArgumentException e) {
            System.err.println("Invalid mode. Only \"split\" or \"calculate\" are allowed. :(");
            printHelpAndExit();
        } finally {
            if (sparkSession != null) {
                sparkSession.stop();
            }
        }
    }
}

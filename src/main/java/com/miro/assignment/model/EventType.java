package com.miro.assignment.model;

public enum EventType {
    registered,
    app_loaded;

    public String getParquetFilename() {
        return String.format("%s.parquet", this.toString());
    }
}


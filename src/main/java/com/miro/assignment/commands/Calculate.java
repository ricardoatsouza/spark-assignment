package com.miro.assignment.commands;

import com.miro.assignment.model.EventType;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.types.DataTypes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class Calculate {

    private final static Calendar calendar = Calendar.getInstance();

    private final static String calendarWeekUdf = "calendarWeekUdf";
    private final static String diffWeekAppLoadedRegisteredUdf = "diffWeekAppLoadedRegisteredUdf";

    public static void calculatePercentage(final SparkSession sparkSession) {
        final Dataset<Row> registeredEventDataset = sparkSession.read().parquet(EventType.registered.getParquetFilename()).cache();
        final Dataset<Row> appLoadedEventDataset = sparkSession.read().parquet(EventType.app_loaded.getParquetFilename()).cache();

        System.out.printf("Percentage: %.5f\n", calculatePercentage(sparkSession, registeredEventDataset, appLoadedEventDataset));
    }

    public static double calculatePercentage(final SparkSession sparkSession, final Dataset<Row> registeredEventDataset, final Dataset<Row> appLoadedEventDataset) {
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        registerUserDefinedFunctions(sparkSession.sqlContext());

        final Dataset<Row> enhancedRegisteredData = registeredEventDataset
                .withColumn(
                        "registeredCalendarWeek",
                        functions.callUDF(calendarWeekUdf, registeredEventDataset.col("timestamp")));

        final Dataset<Row> enhancedAppLoadedData = appLoadedEventDataset
                .withColumn(
                        "appLoadedCalendarWeek",
                        functions.callUDF(calendarWeekUdf, appLoadedEventDataset.col("timestamp")));

        enhancedRegisteredData.createOrReplaceTempView("registeredView");
        enhancedAppLoadedData.createOrReplaceTempView("appLoadedView");

        final Dataset<Row> joinedDataset = enhancedRegisteredData.join(enhancedAppLoadedData, "initiator_id");

        final Dataset<Row> result = joinedDataset
                .withColumn("diffWeekAppLoadedRegistered",
                        functions.callUDF(diffWeekAppLoadedRegisteredUdf,
                                joinedDataset.col("registeredCalendarWeek"),
                                joinedDataset.col("appLoadedCalendarWeek")))
                .filter("diffWeekAppLoadedRegistered = 1");

        final long totalAppLoaded = enhancedAppLoadedData.count();
        final long totalAppLoadedWeekAfterRegistration = result.count();

        return (double) totalAppLoadedWeekAfterRegistration /  totalAppLoaded * 100;
    }

    private static void registerUserDefinedFunctions(final SQLContext sqlContext) {
        sqlContext.udf().register(calendarWeekUdf, (UDF1<String, Object>) Calculate::getYearCalendar, DataTypes.IntegerType);
        sqlContext.udf().register(diffWeekAppLoadedRegisteredUdf, (UDF2<Integer, Integer, Object>) Calculate::diffWeekAppLoadedRegistered, DataTypes.IntegerType);
    }

    private static int getYearCalendar(final String timestamp) throws ParseException {
        // SimpleDateFormat is not thread safe, therefore creating it as local variable
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSX");

        calendar.setTime(dateFormat.parse(timestamp));
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    private static int diffWeekAppLoadedRegistered(final Integer registeredWeek, final Integer appLoadedWeek) {
        return appLoadedWeek - registeredWeek;
    }

}

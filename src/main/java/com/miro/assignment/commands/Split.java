package com.miro.assignment.commands;

import com.miro.assignment.model.EventType;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Map;

public abstract class Split {

    private final static Map<EventType, String[]> columnsToDrop= Map.of(
        EventType.registered, new String[]{"device_type", "browser_version", "campaign"},
        EventType.app_loaded, new String[]{"campaign", "channel", "browser_version"}
    );

    public static void splitAndSaveDatabaseByEvent(final SparkSession sparkSession, final String filename) {
        final Dataset<Row> rawEventData = sparkSession.read().json(filename).cache();

        for (EventType eventType : EventType.values()) {
            final String parquetFilename = eventType.getParquetFilename();
            filterDataset(rawEventData, eventType).write().mode(SaveMode.Overwrite).parquet(parquetFilename);
        }
    }

    public static Dataset<Row> filterDataset(final Dataset<Row> dataset, final EventType eventType) {
        return dataset
                .filter(String.format("event = '%s'", eventType.toString()))
                .drop(columnsToDrop.get(eventType));
    }

}

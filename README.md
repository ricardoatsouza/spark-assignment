Spark-Assignment
================

This assignment runs over a specific given dataset. This dataset is a collection of events, each line a JSON object, 
containing `initiator_id`, `timetamp`, `event` and fields related to the particular event. There are two types of events:

- `registered`: whenever a user (a.k.a. `initiator_id`) registers in the app.
- `app_loaded`: whenever a user opens the app.

The spark application in this repo executes two operations on top of this dataset:

1. Splits the dataset in two smaller datasets in the parquet format.
2. Calculates the percentage of users that opened the app one calendar week after registration.

The execution of the first operation will split the dataset into two smaller files: `registered.parquet` and `app_loaded.parquet`.
The second operations runs over these two files.

## Compile

```bash
$ mvn compile
```

## Tests

```bash
$ mvn test
```

## Generate Jar

```bash
$ mvn package
```

## To run

Make sure to have a Spark cluster running. Generate the Jar file using the mvn command and submit the application to Spark using `./spark-submit`.

To split the dataset into the parquet files, run:

```bash
./spark-submit \
    --master <spark-cluster>> \
    --class com.miro.assignment.Main \
    <jar-file> split <dataset-file>
```

To calculate the percentage, run:

```bash
./spark-submit \
    --master <spark-cluster>> \
    --class com.miro.assignment.Main \
    <jar-file> calculate
```